#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <netdb.h>
#include <unistd.h>
#include <signal.h>
#include <assert.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

int main(int argc, char ** argv){

        int server_port = 49153;
        char * name = argv[1];
        int n; //store the read int
        char buff[256]; //the buffer to store input and output

        //socket and server address
        int sock = socket(AF_INET, SOCK_STREAM,0);
        struct sockaddr_in server;
 	//these are built in variables in the sockadder_in class
        server.sin_family = AF_INET;
        //use localhost because the file is hosted on pilot
        server.sin_addr.s_addr = inet_addr("127.0.0.1");
        server.sin_port = htons(server_port);

        //printf("%d", sock);

        //establish connection to the server
        if (connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0){
                perror("connection failed");
                exit(1);
        }

        //read the initial server message
        n = read(sock, buff, 255);
        printf("%s\n",buff);

        //send your name to server
        printf("Please enter your name: ");
        bzero(buff,256);
        fgets(buff,255,stdin);
        n = write(sock,buff,strlen(buff));
        bzero(buff,256);

        //pause before continuing to read
        sleep(1);
        n = read(sock, buff, 255);
        bzero(buff,256);

 while(1){

                //see if there is any info to be read from the socket
                int toRead;
                ioctl(sock, FIONREAD, &toRead);

                //if there is data to read, display it
                if (toRead != 0) {
                        bzero(buff, 256);
                        n = read(sock,buff,255);
                        printf("%s",buff);
                }

                //recieve user input
                printf("Enter message: ");
                bzero(buff,256);
                fgets(buff,255,stdin);
                if(strlen(buff) > 1) {
                        n = write(sock, buff, strlen(buff));
                        bzero(buff,256);
                        sleep(1);
                }
        }
        close(sock);
        return 0;
}